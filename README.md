APIs exposed to create stock for apples :-
Type: POST
URL: http://localhost:8080/api/appleStock/saveAppleStock

Sample Request:
{
	"stockType" : "HYBRID",
	"quantity" : "10"
}

API to fetch all the Apple stock :-
Type: GET
URL: http://localhost:8080/api/appleStock/getAppleStock

In Memory database has been used for creating the application. Once stock for apples has been created , order can be made for the stock via the following API:-
Ype: POSTURL: http://localhost:8080/api/orders/createOrder

Sample Request:
{
	"orderDetailsDTOs":
		[{
			
				"stockType" : "HYBRID",
				"quantity" : "3",
				"price" : "50"
				
			}],
			
	"customerName" : "vishesh"
		
}

Following Stock Types for Apple has been deined initially:
DEFAULT,HYBRID,ORGANIC

Versioning has been done on AppleStock Entity to implement Optimistic Locking as well as ETag filter has be added to the application to handle concurrent requests for order.
