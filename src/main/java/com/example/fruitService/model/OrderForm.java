package com.example.fruitService.model;

import java.util.List;

public class OrderForm {

    private List<OrderDetailsDTO> orderDetailsDTOs;
    private String customerName;


    public List<OrderDetailsDTO> getOrderDetailsDTOs() {
        return orderDetailsDTOs;
    }

    public void setOrderDetailsDTOs(List<OrderDetailsDTO> orderDetailsDTOs) {
        this.orderDetailsDTOs = orderDetailsDTOs;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
