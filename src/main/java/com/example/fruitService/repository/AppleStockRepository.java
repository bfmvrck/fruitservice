package com.example.fruitService.repository;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.entity.StockType;
import org.springframework.data.repository.CrudRepository;

public interface AppleStockRepository extends CrudRepository<AppleStock, Long> {

    AppleStock getAppleStockByStockType(StockType stockType);

}
