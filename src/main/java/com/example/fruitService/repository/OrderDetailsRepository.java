package com.example.fruitService.repository;

import com.example.fruitService.entity.OrderDetails;
import org.springframework.data.repository.CrudRepository;

public interface OrderDetailsRepository extends CrudRepository<OrderDetails,Long> {
}
