package com.example.fruitService.repository;

import com.example.fruitService.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order,Long> {
}
