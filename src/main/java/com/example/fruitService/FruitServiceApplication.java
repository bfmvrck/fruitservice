package com.example.fruitService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

@SpringBootApplication
public class FruitServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FruitServiceApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean<ShallowEtagHeaderFilter> shallowEtagHeaderFilter() {
		FilterRegistrationBean<ShallowEtagHeaderFilter> filterRegistrationBean
				= new FilterRegistrationBean<>( new ShallowEtagHeaderFilter());
		filterRegistrationBean.addUrlPatterns("/api/orders/*");
		filterRegistrationBean.setName("etagFilter");
		return filterRegistrationBean;
	}

}
