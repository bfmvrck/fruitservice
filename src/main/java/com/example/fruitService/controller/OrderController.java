package com.example.fruitService.controller;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.entity.Order;
import com.example.fruitService.entity.OrderDetails;
import com.example.fruitService.entity.StockType;
import com.example.fruitService.exception.StockTypeNotFoundException;
import com.example.fruitService.model.OrderDetailsDTO;
import com.example.fruitService.model.OrderForm;
import com.example.fruitService.service.AppleStockService;
import com.example.fruitService.service.OrderDetailsService;
import com.example.fruitService.service.OrderService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    AppleStockService appleStockService;
    OrderService orderService;
    OrderDetailsService orderDetailsService;

    public OrderController(AppleStockService appleStockService, OrderService orderService, OrderDetailsService orderDetailsService) {
        this.appleStockService = appleStockService;
        this.orderService = orderService;
        this.orderDetailsService = orderDetailsService;
    }

    @PostMapping(value = "/createOrder")
    public Order createOrder(@RequestBody final OrderForm orderForm) {
        List<OrderDetailsDTO> orderDetailsDTOs = orderForm.getOrderDetailsDTOs();
        orderDetailsService.validateOrderDetailsDTO(orderDetailsDTOs);
        Order order = new Order();
        order.setCustomerName(orderForm.getCustomerName());
        orderService.create(order);
        List<OrderDetails> orderDetails = new ArrayList<>();
        for (OrderDetailsDTO orderDetailsDTO : orderDetailsDTOs) {
            String stockType = orderDetailsDTO.getStockType();
            AppleStock appleStock = appleStockService.getAppleStockByStockType(stockType);
            Integer quantityLeft = appleStock.getQuantity() - orderDetailsDTO.getQuantity();
            appleStock.setQuantity(quantityLeft);
            appleStockService.save(appleStock);
            orderDetails.add(orderDetailsService.create(new OrderDetails(order, appleStock, orderDetailsDTO.getQuantity(), orderDetailsDTO.getPrice())));
        }
        order.setOrderDetails(orderDetails);
        orderService.update(order);

        return order;
    }


}
