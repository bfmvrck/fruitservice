package com.example.fruitService.controller;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.service.AppleStockService;
import com.sun.istack.NotNull;
import org.springframework.web.bind.annotation.*;

import javax.transaction.SystemException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/appleStock")
public class AppleStockController {

    private AppleStockService appleStockService;

    public AppleStockController(AppleStockService appleStockService){
        this.appleStockService = appleStockService;
    }

    @GetMapping(value = { "", "/" })
    public @NotNull
    Iterable<AppleStock> getAppleStocks() {
        return appleStockService.getAllAppleStocks();
    }

    @PostMapping(value = "/getAppleStock")
    public AppleStock getAppleStock(@RequestBody final String stockType) throws SystemException {
        return appleStockService.getAppleStockByStockType(stockType);
    }

    @PostMapping(value = "/saveAppleStock")
    public AppleStock save(@Valid @RequestBody final AppleStock appleStock) throws SystemException {
        return appleStockService.save(appleStock);
    }

    @PostMapping(value = "/updateAppleStock")
    public AppleStock update(@Valid @RequestBody final AppleStock appleStock) throws SystemException {
        return appleStockService.update(appleStock);
    }
}
