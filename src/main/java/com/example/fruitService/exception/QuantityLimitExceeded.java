package com.example.fruitService.exception;

public class QuantityLimitExceeded extends RuntimeException {

    public QuantityLimitExceeded(String message){
        super(message);
    }
}
