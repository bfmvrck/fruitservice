package com.example.fruitService.exception;

public class StockOverException extends RuntimeException{

    public StockOverException(String message){
        super(message);
    }
}
