package com.example.fruitService.exception;

public class StockTypeNotFoundException extends RuntimeException {

    public StockTypeNotFoundException(String message) {
        super(message);
    }
}
