package com.example.fruitService.service;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.entity.StockType;
import com.example.fruitService.exception.QuantityLimitExceeded;
import com.example.fruitService.exception.StockTypeNotFoundException;
import com.sun.istack.NotNull;
import org.springframework.validation.annotation.Validated;

import javax.transaction.SystemException;
import javax.validation.constraints.Min;

@Validated
public interface AppleStockService {

    @NotNull
    Iterable<AppleStock> getAllAppleStocks();

    AppleStock getAppleStockByStockType(String stockType);

    AppleStock save(AppleStock appleStock);

    AppleStock update(AppleStock appleStock);

    boolean isQuantityValid(AppleStock appleStock,Integer quantity);

}
