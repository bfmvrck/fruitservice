package com.example.fruitService.service;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.entity.StockType;
import com.example.fruitService.exception.QuantityLimitExceeded;
import com.example.fruitService.exception.StockOverException;
import com.example.fruitService.exception.StockTypeNotFoundException;
import com.example.fruitService.repository.AppleStockRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AppleStockServiceImpl implements AppleStockService {

    private AppleStockRepository appleStockRepository;

    public AppleStockServiceImpl(AppleStockRepository appleStockRepository) {
        this.appleStockRepository = appleStockRepository;
    }

    @Override
    public Iterable<AppleStock> getAllAppleStocks() {
        return appleStockRepository.findAll();
    }

    @Override
    public AppleStock getAppleStockByStockType(String stockType) {

        AppleStock appleStock = appleStockRepository.getAppleStockByStockType(getStockType(stockType));
        if (appleStock == null)
            throw new StockOverException(stockType + " not available");

        return appleStock;

    }

    @Override
    public AppleStock save(AppleStock appleStock) {

        AppleStock appleStock1 = appleStockRepository.getAppleStockByStockType(getStockType(appleStock.getStockType().name()));
        if(appleStock1 == null)
            return appleStockRepository.save(appleStock);
        else
            return appleStockRepository.save(appleStock1);

    }

    @Override
    public AppleStock update(AppleStock appleStock) {

        AppleStock appleStock1 = appleStockRepository.getAppleStockByStockType(getStockType(appleStock.getStockType().name()));
        if(appleStock1 == null)
            throw new StockOverException(appleStock.getStockType().name()+" not found. Please create the stock first");
        appleStock1.setQuantity(appleStock1.getQuantity() + appleStock.getQuantity());
        return appleStockRepository.save(appleStock1);
    }

    private StockType getStockType(String stockType) {

        StockType stockType1;
        try {
            stockType1 = StockType.valueOf(stockType);
        } catch (IllegalArgumentException e) {
            throw new StockTypeNotFoundException("Stock Type is invalid");
        }
        return stockType1;
    }

    @Override
    public boolean isQuantityValid(AppleStock appleStock, Integer quantity) throws QuantityLimitExceeded {
        if (appleStock.getQuantity() < quantity)
            throw new QuantityLimitExceeded("Available quantity for " + appleStock.getStockType().name() + " is " + appleStock.getQuantity());
        else
            return true;
    }

}
