package com.example.fruitService.service;

import com.example.fruitService.entity.AppleStock;
import com.example.fruitService.entity.OrderDetails;
import com.example.fruitService.model.OrderDetailsDTO;
import com.example.fruitService.repository.OrderDetailsRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Transactional
@Service
public class OrderDetailsServiceImpl implements OrderDetailsService {

    private AppleStockService appleStockService;
    private OrderDetailsRepository orderDetailsRepository;

    public OrderDetailsServiceImpl(AppleStockService appleStockService,OrderDetailsRepository orderDetailsRepository) {
        this.appleStockService = appleStockService;
        this.orderDetailsRepository = orderDetailsRepository;
    }

    @Override
    public void validateOrderDetailsDTO(List<OrderDetailsDTO> orderDetailsDTOs) {
        orderDetailsDTOs.stream().filter(oddt -> {
            AppleStock appleStock = appleStockService.getAppleStockByStockType(oddt.getStockType());
            return appleStockService.isQuantityValid(appleStock,oddt.getQuantity());
        }).collect(Collectors.toList());

    }

    @Override
    public OrderDetails create(OrderDetails orderDetails) {
        return orderDetailsRepository.save(orderDetails);
    }
}
