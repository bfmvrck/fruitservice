package com.example.fruitService.service;

import com.example.fruitService.entity.Order;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
public interface OrderService {

    @NotNull
    Iterable<Order> getAllOrders();

    Order create(@NotNull(message = "The order cannot be null.") @Valid Order order);

    Order update(@NotNull(message = "The order cannot be null.") @Valid Order order);
}
