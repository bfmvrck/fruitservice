package com.example.fruitService.service;

import com.example.fruitService.entity.Order;
import com.example.fruitService.repository.OrderRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public @NotNull Iterable<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order create(@NotNull(message = "The order cannot be null.") @Valid Order order) {
            order.setOrderDate(LocalDate.now());
            return orderRepository.save(order);
    }

    @Override
    public Order update(@NotNull(message = "The order cannot be null.") @Valid Order order) {
        return orderRepository.save(order);
    }
}
