package com.example.fruitService.service;

import com.example.fruitService.entity.OrderDetails;
import com.example.fruitService.model.OrderDetailsDTO;

import java.util.List;

    public interface OrderDetailsService {

        void validateOrderDetailsDTO(List<OrderDetailsDTO> orderDetailsDTOs);

        OrderDetails create(OrderDetails orderDetails);

    }
